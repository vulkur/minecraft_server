#!/bin/bash

TIMESTAMP=`date +%Y-%m-%d_%H-%M-%S`

cp /opt/minecraft/vanilla/world world
git add -A
git commit -m "World Save: $TIMESTAMP"
git push