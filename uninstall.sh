#!/bin/bash

#disable service
systemctl disable minecraft@vanilla

#delete user
userdel minecraft || true

#delete /opt/minecraft
rm -rf /opt/minecraft > /dev/null 2>&1 || true

#delete server
rm /etc/systemd/system/minecraft@.service > /dev/null 2>&1 || true