#!/bin/bash

sudo ./command.sh say SHUTDOWN in 1:00
sleep 30
sudo ./command.sh say SHUTDOWN in 0:30
sleep 15
sudo ./command.sh say SHUTDOWN in 0:15
sleep 5
sudo ./command.sh say SHUTDOWN in 0:10
sleep 5
sudo ./command.sh say SHUTDOWN in 0:05
sleep 5

systemctl stop minecraft@vanilla
