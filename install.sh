#!/bin/bash

MCVER="1.15.2"
HOME_DIR="/opt/minecraft"
VANILLA="$HOME_DIR/vanilla"

VERSIONS=https://launchermeta.mojang.com/mc/game/version_manifest.json
MANIFEST="manifest.json"
VERSION_MANIFEST="version.json"

sudo apt install openjdk-11-jre screen jq -y

# create /opt
mkdir -p /opt

# add minecraft user
adduser --system --shell /bin/bash --home $HOME_DIR --group minecraft > /dev/null 2>&1 || true

# create vanilla dir
mkdir -p $VANILLA
chown minecraft:minecraft $VANILLA

# sign eula
echo "eula=true" > $VANILLA/eula.txt
chown minecraft:minecraft $VANILLA/eula.txt
echo "eula signed"

# copy service.
cp minecraft@.service /etc/systemd/system/minecraft@.service
echo "service copied"

#download server
echo "downloading server . . ."
curl -o $MANIFEST $VERSIONS
LATEST_VERSION_URL=$(jq ".versions[] | select(.id==$(jq '.latest.release' $MANIFEST)).url" $MANIFEST)

	# trim quotes
	LATEST_VERSION_URL="${LATEST_VERSION_URL%\"}"
	LATEST_VERSION_URL="${LATEST_VERSION_URL#\"}"

#get server.jar download url
curl -o $VERSION_MANIFEST $LATEST_VERSION_URL
DOWNLOAD_URL=$(jq '.downloads.server.url' $VERSION_MANIFEST)

	# trim quotes
	DOWNLOAD_URL="${DOWNLOAD_URL%\"}"
	DOWNLOAD_URL="${DOWNLOAD_URL#\"}"

# download jar
curl -o $VANILLA/vanilla.jar $DOWNLOAD_URL
chown minecraft:minecraft $VANILLA/vanilla.jar
echo "server downloaded"

# cleanup
rm $MANIFEST
rm $VERSION_MANIFEST

#enable service
systemctl enable minecraft@vanilla
